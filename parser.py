from bs4 import BeautifulSoup
import urllib
import sqlite3
import datetime

conn = sqlite3.connect('database')
conn.text_factory = str
page = urllib.urlopen('http://193.95.233.105/econova1/Default.aspx?mesto=Koper');
pagesoup = BeautifulSoup(page)

page_title = str(pagesoup.find(id="MainContent_Label14").get_text().encode('utf-8'))
air_temp = str(pagesoup.find(id="MainContent_Label1").get_text().encode('utf-8'))
wind_data = str(pagesoup.find(id="MainContent_Label5").get_text().encode('utf-8'))
humidity_data = str(pagesoup.find(id="MainContent_Label2").get_text().encode('utf-8'))
air_pressure = str(pagesoup.find(id="MainContent_Label4nova").get_text().encode('utf-8'))
solar_power = str(pagesoup.find(id="MainContent_Label3").get_text().encode('utf-8'))

timedate = datetime.datetime.now()
time = timedate.hour
conn.execute("INSERT INTO weather (time, air_temp, wind_data, humidity_data, air_pressure, solar_power) VALUES (?, ?, ?, ?, ?, ?)", (time, air_temp, wind_data, humidity_data, air_pressure, solar_power))
conn.commit()

print page_title
print air_temp
print wind_data
print humidity_data
print air_pressure
print solar_power